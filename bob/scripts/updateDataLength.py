# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# ETALON Multiline 2
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory

import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def generatorProcedure():
    #logger.info("generatorProcedure")
    # -------------------------------------------------------------------------
    # This Python script is attached to a display
    # and triggered by loc://initial_trigger$(DID)(1)
    # to execute once when the display is loaded.
    # -------------------------------------------------------------------------
    try:
        # ---------------------------------------------------------------------
        # Input PVs
        # ---------------------------------------------------------------------
        # pv0 = PVUtil.getString(pvs[0])
        #logger.info("started....")
        WaveFormData = PVUtil.getTable(pvs[0])              # $(DEV):Data:$(LATEST_LENGTH_PV):Channel$(CHANNEL)
        # ---------------------------------------------------------------------
        # Latest data length for a channel
        # ---------------------------------------------------------------------
        # [ 0] number of measured channels
        # [ 1] measured lengths for all channels
        # [ 2] measured temperatures for all channels
        # [ 3] measured pressure for all channels
        # [ 4] measured humidity for all channels
        # [ 5] measured intensities for all channels
        # [ 6] analysis error flag                      (0, 1)
        # [ 7] beam break error flag                    (0, 1)
        # [ 8] intensity error flag                     (0, 1)
        # [ 9] gas cell temperature break error flag    (0, 1)
        # [10] motion tolerance error flag              (0, 1)
        # [11] internal DLL error flag                  (0, 1)
        # [12] USB connection error flag                (0, 1)
        # [13] laser speed error flag                   (0, 1)
        # [14] laser temperature error flag             (0, 1)
        # [15] DAQ error flag                           (0, 1)
        # ---------------------------------------------------------------------
        #logger.info("WaveFormData: %s" % str(WaveFormData))
        #logger.info("WaveFormData[0]: %s" % str(WaveFormData[0]))
        # ---------------------------------------------------------------------
        # Parsing waveform (NELM = 17) in individual PVs::
        # ---------------------------------------------------------------------
        # loc://Data:$(LATEST_LENGTH_PV):Channel$(CHANNEL)
        # loc://Data:$(LATEST_LENGTH_PV):Lengths$(CHANNEL)
        # loc://Data:$(LATEST_LENGTH_PV):Temp$(CHANNEL)
        # loc://Data:$(LATEST_LENGTH_PV):Pressure$(CHANNEL)
        # loc://Data:$(LATEST_LENGTH_PV):Humidity$(CHANNEL)
        # loc://Data:$(LATEST_LENGTH_PV):Intens$(CHANNEL)
        # loc://Data:$(LATEST_LENGTH_PV):AnalysisError$(CHANNEL)        (?)
        # loc://Data:$(LATEST_LENGTH_PV):BeamBreakError$(CHANNEL)       (0, 1)
        # loc://Data:$(LATEST_LENGTH_PV):IntensError$(CHANNEL)          (0, 1)
        # loc://Data:$(LATEST_LENGTH_PV):TempError$(CHANNEL)            (0, 1)
        # loc://Data:$(LATEST_LENGTH_PV):MotionTolError$(CHANNEL)       (0, 1)
        # loc://Data:$(LATEST_LENGTH_PV):DllError$(CHANNEL)             (0, 1)
        # loc://Data:$(LATEST_LENGTH_PV):UsbError$(CHANNEL)             (0, 1)
        # loc://Data:$(LATEST_LENGTH_PV):LaserSpeedError$(CHANNEL)      (0, 1)
        # loc://Data:$(LATEST_LENGTH_PV):LaserTempError$(CHANNEL)       (0, 1)
        # loc://Data:$(LATEST_LENGTH_PV):DaqError$(CHANNEL)             (0, 1)
        # ---------------------------------------------------------------------

        # ---------------------------------------------------------------------
        # BEFORE refactoring
        # ---------------------------------------------------------------------
        # for i in range(1,17):
        #     pvs[i].setValue(WaveFormData[i-1][0])
        # ---------------------------------------------------------------------
        # AFTER refactoring
        # ---------------------------------------------------------------------
        pvs[1].setValue(WaveFormData[0][0])
    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
generatorProcedure()
